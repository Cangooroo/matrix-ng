package matrix.core;

import java.util.LinkedList;

/**
 * Abstract base class for modules.
 * The class has a {@link MessageQueue}. Ancestors should implement the
 * {@link Runnable} interface.
 */
abstract public class Module implements Runnable, Configurable {

	protected final LoggerProxy log;

	Thread thread = null;

	boolean defunct = false;

	boolean hasToStop = false;

	/**
	 * The module's own {@link MessageQueue}.
	 */
	protected final MessageQueue messageQueue = new MessageQueue();

	protected Module(LoggerProxy loggerProxy) {
		this.log = loggerProxy;
	}

	/**
	 * Offer a {@link Message} to the module.
	 * @param message The {@link Message}
	 * @return See {@link MessageQueue#addMessage}
	 */
	boolean receiveMessage(final Message message) {
		return messageQueue.addMessage(message);
	}

	protected void sendMessage(final Message message) {
		Core.getInstance().sendMessage(message);
	}

	protected void updateMessageSubscription(LinkedList<String> filters) {
		Core.getInstance().updateMessageSubscription(filters);
	}

	final void start() {
		if (thread != null && thread.isAlive()) {
			log.error("Something went wrong, the module's thread is still/already alive, not starting the module.");
		} else {
			log.verbose("Creating thread for module.");
			thread = new Thread(this);
			thread.start();
		}
	}

	final void stop() {
		this.hasToStop = true;
		// Some modules (MessageHandlers definitely) has to see this and shut down properly
		log.verbose("Waiting for module to stop...");
		try {
			thread.join(2000);
		} catch (InterruptedException e) {
			// We don't care about this
		}
		if (thread.isAlive()) {
			thread.interrupt();
			log.verbose("Sent interrupt, waiting for module to stop...");
			try {
				thread.join(2000);
			} catch (InterruptedException e) {
				// We don't care about this
			}
		}
	}

	/**
	 * Main method for the {@link Runnable}, that calls {@link #mainLoop()}.
	 */
	@Override
	public final void run() {
		try {
			mainLoop();
		} catch (InterruptedException e) {
			if (!this.hasToStop) {
				log.error("Module interrupted. Finalising...");
				log.error(e);
			}
		}
		log.information("Module exited, finalising...");
		finalise();
		log.information("Module finished.");
	}

	/**
	 * Initialisation of the module.
	 * @return True on success, false otherwise.
	 */
	protected abstract boolean initialise();

	/**
	 * Main thread of the module.
	 */
	protected abstract void mainLoop() throws InterruptedException;

	/**
	 * Finalisation of the module.
	 * @return True on success, false otherwise.
	 */
	protected abstract boolean finalise();

}
