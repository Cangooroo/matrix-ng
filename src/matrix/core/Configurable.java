package matrix.core;

public interface Configurable {

	/**
	 * Update the configuration.
	 * @param configuration The relevant part of the global configuration.
	 * @return True on success, false otherwise.
	 */
	public boolean updateConfiguration(Configuration configuration);
}
