package matrix.core;

	/**
	 * Abstract base class for filters.
	 * Ancestors should implement the {@link Runnable} interface.
	 */
abstract public class Filter implements Runnable, Configurable {

	private final LoggerProxy log;

	/**
	 * The filter's {@link MessageQueue} where it awaits messages to filter.
	 */
	private MessageQueue messageQueue = new MessageQueue();

	private Thread thread = null;

	private boolean hasToStop = false;

	/**
	 * The type of messages to filter
	 */
	private String messageType = null;


	/**
	 * Indicates whether the filter is "soft" (running in the thread of the caller module)
	 * or "hard" (running in its own thread)
	 */
	private boolean soft = true;

	protected Filter(LoggerProxy loggerProxy) {
		this.log = loggerProxy;
	}


	/**
	 * Sets the type of message to filter.
	 * @param messageType The string representation of the message type
	 */
	protected void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	/**
	 * Gets whether the filter is "soft"
	 * (running in the thread of the caller module)
	 */
	public boolean isSoft() {return soft;}

	/**
	 * Gets whether the filter is "hard"
	 * (running in its own thread)
	 */
	boolean isHard() {return !soft;}


	/**
	 * Sets the filter to "soft"
	 * (running in the thread of the caller module)
	 */
	public void makeSoft() {
		log.verbose("Setting filter to soft");
		if (soft) {
			log.verbose("Filter is already soft");
			return;
		}
		soft = true;
		stopThread();
	}


	/**
	 * Sets the filter to "hard"
	 * (running in its own thread)
	 */
	public void makeHard() {
		log.verbose("Setting filter to hard");
		if (!soft) {
			log.verbose("Filter is already hard");
			return;
		}
		soft = false;
		if (thread != null && thread.isAlive()) {
			log.error("Something went wrong, the filter's thread is already alive");
		} else {
			log.verbose("Creating thread for filter.");
			thread = new Thread(this);
			thread.start();
		}
	}

	/**
	 * Function to filter the message.
	 * @param message The {@link Message}
	 * @return the filtered {@link Message}
	 */
	protected Message filterMessage(final Message message) {
		return null;
	}

	/**
	 * Gets which type of message the filter filters
	 * @return
	 */
	public final String getMessageType() {
		return messageType;
	}

	/**
	 * Gracefully stops the filter
	 * @return whether the stop was successful
	 */
	public final boolean stop() {
		if (isHard()) stopThread();
		return true;
	}


	/**
	 * Adds a message to the filters queue
	 * @param message The {@link Message}
	 */
	public void addMessageToFilter(Message message) {
		messageQueue.addMessage(message);
	}


	/**
	 * Stops the filter's thread
	 */
	private void stopThread() {
		if (thread == null || !thread.isAlive()) {
			log.error("Something went wrong, the filter's thread is already stopped");
			return;
		}
		this.hasToStop = true;
		log.verbose("Waiting for filter's thread to stop.");
		try {
			thread.join(3000);
		} catch (InterruptedException e) {
			// We don't care about this
		}
		thread.interrupt();
		thread = null;
	}

	/**
	 * The run method where the "hard" filters process their messages
	 */
	@Override
	public final void run() {
		while (!hasToStop) {
			try {
				Message message = messageQueue.waitAndRemoveMessage();
				Core.getInstance().sendMessage(filterMessage(message), true);
			} catch (InterruptedException e) {
				// We don't care, the while loop should exit if we have to stop
			}
		}
	}
}
