package matrix.core;

import matrix.core.Event.Severity;

/**
 * A proxy for a {@link Logger} object, that sets the extra {@code sourceName}
 * parameter to a predefined argument set in the constructor of the object.
 */
public class LoggerProxy {

	/**
	 * The reference to the {@link Logger} object to forward {@link Event}s.
	 */
	private final Logger logger;

	/**
	 * The {@code sourceName parameter} to send as the extra argument for
	 * {@link Logger#logEvent}.
	 */
	private final String sourceName;

	/**
	 * Constructor.
	 * @param logger The associated {@link Logger} object.
	 * @param sourceName The extra argument to be sent to the {@link Logger}.
	 */
	public LoggerProxy(Logger logger, String sourceName) {
		this.logger = logger;
		this.sourceName = sourceName;
	}

	/**
	 * Log the {@link Event} through the {@link Logger}, and set the extra
	 * {@code sourceName} parameter.
	 * @param event The {@link Event} to be logged.
	 */
	void logEvent(Event event) {
		this.logger.logEvent(this.sourceName, event);
	}

	public void fatal(String message) {
		logEvent(new Event(Severity.FATAL, message));
	}
	public void fatal(Exception exception) {
		logEvent(new Event(Severity.FATAL, exception));
	}

	public void error(String message) {
		logEvent(new Event(Severity.ERROR, message));
	}
	public void error(Exception exception) {
		logEvent(new Event(Severity.ERROR, exception));
	}

	public void warning(String message) {
		logEvent(new Event(Severity.WARNING, message));
	}
	public void warning(Exception exception) {
		logEvent(new Event(Severity.WARNING, exception));
	}

	public void notification(String message) {
		logEvent(new Event(Severity.NOTIFICATION, message));
	}
	public void notification(Exception exception) {
		logEvent(new Event(Severity.NOTIFICATION, exception));
	}

	public void information(String message) {
		logEvent(new Event(Severity.INFORMATION, message));
	}
	public void information(Exception exception) {
		logEvent(new Event(Severity.INFORMATION, exception));
	}

	public void verbose(String message) {
		logEvent(new Event(Severity.VERBOSE, message));
	}
	public void verbose(Exception exception) {
		logEvent(new Event(Severity.VERBOSE, exception));
	}

	public void debug(String message) {
		logEvent(new Event(Severity.DEBUG, message));
	}
	public void debug(Exception exception) {
		logEvent(new Event(Severity.DEBUG, exception));
	}

}
