package matrix.core;

public class Message {

	private static Long nextID = 1l;

	/**
	 * The type of the message.
	 * Messages can be categorised by their type. Their type is a dot separated list of words, like Java package names.
	 */
	public final String type;

	/**
	 * The ID of the message.
	 * Within this instance, this ID uniquely identifies the message.
	 */
	public final long ID;

	/**
	 * The ID of the message this message replies to.
	 * If it is zero, this message is not a reply.
	 */
	long RID;

	/**
	 * Create a new message with the given attributes.
	 * @param type The type of the message.
	 */
	protected Message(String type) {
		synchronized (Message.nextID) {
			this.type = type;
			this.ID = Message.nextID++;
			this.RID = 0;
		}
	}

	/**
	 * Get the RID (reply to ID) of this Message.
	 * @return {@link #RID}
	 */
	public long getRID() {
		return RID;
	}

	/**
	 * Set the RID (reply to ID) of this Message.
	 * @param message The Message this one replies to.
	 * @return This Message.
	 */
	public Message replyTo(Message message) {
		RID = message.ID;
		return this;
	}

}
