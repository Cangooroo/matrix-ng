package matrix.core;

public abstract class MessageHandlerModule extends Module {

	MessageHandlerModule(LoggerProxy loggerProxy) {
		super(loggerProxy);
	}

	@Override
	protected final void mainLoop() {
		while (!hasToStop) {
			try {
				Message message = messageQueue.waitAndRemoveMessage();
				if (!handleMessage(message)) {
					log.warning("Could not handle a message of type " + message.type + ".");
				}
			} catch (InterruptedException e) {
				// We don't care, the while loop should exit if we have to stop
			}
		}
	}

	protected abstract boolean handleMessage(Message message);
}
