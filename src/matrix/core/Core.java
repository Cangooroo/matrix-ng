package matrix.core;

import sun.misc.Signal;
import sun.misc.SignalHandler;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

class Core implements SignalHandler {

	/**
	 * Entry point for the application. This method prepares and starts the
	 * mainLoop thread.
	 *
	 * @param arguments Command line arguments.
	 */
	public static void main(String[] arguments) {
		// Check for the one argument we need.
		if (arguments.length != 1) {
			System.err.println("Usage: matrix-ng <configuration-file>");
			System.exit(-1);
		} else {
			// Load configuration
			File configurationFile = new File(arguments[0]);
			Configuration configuration = ConfigurationParser.loadConfiguration(configurationFile);
			if (configuration == null) {
				System.err.println("Invalid configuration. Exiting.");
				System.exit(1);
			}
			// Create the Core instance and pass the configuration
			Core.instance = new Core(configurationFile, configuration);
			Core.instance.main();
			System.err.println("ERROR: Core did not exit properly.");
			System.exit(2);
		}
	}

	/**
	 * The singleton instance of the class.
	 */
	private static Core instance;

	static Core getInstance() {
		return instance;
	}

	private File configurationFile = null;

	private Configuration configuration = null;

	private final Logger logger;

	private final LoggerProxy log;

	private final Module coreModule;

	private LinkedList<String> coreModuleFilters = new LinkedList<>();

	private HashMap<String, Module> modules = new HashMap<>();

	private HashMap<String, Filter> filters = new HashMap<>();

	private HashMap<String, LinkedList<String>> moduleFilters = new HashMap<>();

	private HashMap<String, LinkedList<String>> moduleFiltersCache = new HashMap<>();

	private Core(File configurationFile, Configuration configuration) {
		this.configurationFile = configurationFile;
		this.configuration = configuration;
		this.logger = new Logger();
		this.logger.updateConfiguration(this.configuration.getSubset("Core.Logger"));
		this.log = new LoggerProxy(this.logger, "Core");
		this.coreModule = new CoreModule(new LoggerProxy(this.logger, "Core Module"));
	}

	private void main() {
		// Register handled signals
		this.registerSignals();
		// Dump all configuration
		dumpConfiguration();
		// Load enabled modules
		loadModules();
		// Load enabled filters
		loadFilters();
		// Prepare modules
		updateModulesConfiguration();
		// Prepare filters
		updateFiltersConfiguration();
		initialiseModules();
		// Start modules
		startModules();
		// Start Core Module
		coreModule.updateConfiguration(this.configuration.getSubset("Core"));
		coreModule.initialise();
		coreModule.start();
		// Wait for the core module
		while (coreModule.thread.isAlive()) {
			try {
				coreModule.thread.join();
			} catch (InterruptedException e) {
				// Who cares?
			}
		}
		System.err.println("ERROR: Core Module did not exit properly.");
		System.exit(3);
	}

	//
	// Message handling
	//
	/**
	 * Subscribe to messages. A module can call this method anytime to set or
	 * update its filters on message subscription.
	 *
	 * @param filters The {@link LinkedList} of filters.
	 */
	void updateMessageSubscription(LinkedList<String> filters) {
		String moduleName = (new Exception()).getStackTrace()[2].getClassName().replaceAll("[^\\.]+\\.", "");
		if (modules.containsKey(moduleName)) {
			this.moduleFilters.put(moduleName, filters);
			this.moduleFiltersCache.clear();
			log.verbose("Updated message subscription filters for module " + moduleName + ".");
			return;
		} else if (moduleName.equals("CoreModule")) {
			this.coreModuleFilters = filters;
			return;
		}
		log.error("Could not update message subscription filters, as " + moduleName + " is not a valid module name.");
	}

	/**
	 * Distribute a message among modules.
	 *
	 * @param message The {@link Message} to be distributed.
	 */
	void sendMessage(Message message) {
		sendMessage(message, false);
	}

	/**
	 * Distribute a message among modules.
	 *
	 * @param message The {@link Message} to be distributed.
	 * @param filtered Whether a filter has been already applied to the
	 * {@link Message}.
	 */
	void sendMessage(Message message, boolean filtered) {
		log.debug("Distributing a message: ID = " + message.ID + ", Type = " + message.type + ", RID = " + message.RID + ".");
		if (!filtered) {
			for (Map.Entry<String, Filter> filterEntry : filters.entrySet()) {
				Filter filter = filterEntry.getValue();
				if (filter.getMessageType().equals(message.type)) {
					if (filter.isSoft()) {
						message = filter.filterMessage(message);
						log.debug("Sent the message through filter " + filterEntry.getKey() + ".");
					} else {
						filter.addMessageToFilter(message);
						log.debug("Added the message to filter " + filterEntry.getKey() + "'s MessageQueue.");
						return;
					}
				}
			}
		}
		LinkedList<String> cachedEntry = null;
		if ((cachedEntry = moduleFiltersCache.get(message.type)) != null) {
			// If we have a cache
			for (String moduleName : cachedEntry) {
				boolean result = false;
				if (moduleName.equals("CoreModule")) {
					result = coreModule.receiveMessage(message);
				} else {
					result = modules.get(moduleName).receiveMessage(message);
				}
				if (!result) {
					log.warning("Could not send a message to " + moduleName + ".");
				} else {
					log.debug("Send the message to " + moduleName + ".");
				}
			}
		} else {
			// We don't have a cache... :(
			this.moduleFiltersCache.put(message.type, new LinkedList<String>());
			// For every module's filter list
			for (Map.Entry<String, LinkedList<String>> filterEntry : moduleFilters.entrySet()) {
				String moduleName = filterEntry.getKey();
				LinkedList<String> filterList = filterEntry.getValue();
				// For every filter
				for (String filter : filterList) {
					// If the filter matches, send a copy of the message to the module
					if (message.type.matches(filter)) {
						boolean result = modules.get(moduleName).receiveMessage(message);
						if (!result) {
							log.warning("Could not send a message to " + moduleName);
						} else {
							log.debug("Send the message to " + moduleName + ".");
						}
						// Add to the cache!
						this.moduleFiltersCache.get(message.type).add(moduleName);
					}
				}
			}
			// And for the CoreModule
			String moduleName = "CoreModule";
			// For every filter
			for (String filter : coreModuleFilters) {
				// If the filter matches, send a copy of the message to the module
				if (message.type.matches(filter)) {
					boolean result = coreModule.receiveMessage(message);
					if (!result) {
						log.error("Could not send a message to " + moduleName);
					} else {
						log.debug("Send the message to " + moduleName + ".");
					}
					// Add to the cache!
					this.moduleFiltersCache.get(message.type).add(moduleName);
				}
			}
		}
	}

	// TODO: defunct modules
	//
	// Module/filter loading
	//
	void loadModules() {
		loadModulesOrFilters(false);
	}

	void loadFilters() {
		loadModulesOrFilters(true);
	}

	void loadModulesOrFilters(boolean isFilter) {
		String matchStr = isFilter ? "enableFilter" : "enable";
		for (Map.Entry<String, String> entry : configuration.getMatching(".+\\." + matchStr).entrySet()) {
			// If enabled, try to load
			if (Configuration.getBooleanValue(entry.getValue())) {
				// Extract the name
				String fullName = entry.getKey().replaceFirst("\\." + matchStr, "");
				loadModuleOrFilter(fullName, isFilter);
			}
		}
	}

	@SuppressWarnings("unchecked")
	void loadModuleOrFilter(String fullName, boolean isFilter) {
		String name = fullName.replaceAll("[^\\.]+\\.", "");
		try {
			// See: http://stackoverflow.com/questions/6197802/typesafe-forname-class-loading
			// I did not find any better way to do this :(
			if (isFilter) {
				if (filters.containsKey(name)) {
					log.warning("Filter " + name + "(" + fullName + ") is already loaded.");
				} else {
					Class<Filter> filterClass = (Class<Filter>) Class.forName("matrix.filters." + fullName);
					Constructor<Filter> constructor = filterClass.getConstructor(LoggerProxy.class);
					Filter filter = constructor.newInstance(new LoggerProxy(logger, name));
					filters.put(name, filter);
					log.information("Loaded filter " + name + "(" + fullName + ").");
					if (configuration.getBoolean(fullName + ".hard")) {
						filter.makeHard();
						log.information("Set filter " + name + " to hard.");
					} else {
						filter.makeSoft();
						log.information("Set filter " + name + " to soft.");
					}
				}
			} else {
				Class<Module> moduleClass = (Class<Module>) Class.forName("matrix.modules." + fullName);
				Constructor<Module> constructor = moduleClass.getConstructor(LoggerProxy.class);
				Module module = constructor.newInstance(new LoggerProxy(logger, name));
				if (modules.containsKey(name)) {
					log.warning("Module " + name + "(" + fullName + ") is already loaded. Replacing...");
					stopModule(name);
					unloadModule(name);
				}
				modules.put(name, module);
				log.information("Loaded module " + name + "(" + fullName + ").");
			}
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
			log.error("Could not load module '" + fullName + "'.");
			log.error(e);
			e.printStackTrace();
		}
	}

	void unloadModule(String moduleName) {
		stopModule(moduleName);
		modules.remove(moduleName);
	}

	//
	// Configuration
	//
	void updateModulesConfiguration() {
		for (Map.Entry<String, Module> entry : modules.entrySet()) {
			String moduleName = entry.getKey();
			updateModuleOrFilterConfiguration(moduleName, false);
		}
	}

	void updateFiltersConfiguration() {
		for (Map.Entry<String, Filter> entry : filters.entrySet()) {
			String filterName = entry.getKey();
			updateModuleOrFilterConfiguration(filterName, true);
		}
	}

	boolean updateModuleOrFilterConfiguration(String name, boolean isFilter) {
		Configurable configurable = isFilter ? filters.get(name) : modules.get(name);
		String confType = isFilter ? "filter" : "module";
		log.information("Initialising " + confType + " " + name + ".");
		if (configurable.updateConfiguration(configuration.getSubset(name))) {
			return true;
		} else {
			log.error("Failed to update configuration for " + confType + " " + name + ".");
			if (configurable instanceof Module) {
				((Module) configurable).defunct = true;
			}
			return false;
		}
	}

	//
	// Module initialisation
	//
	void initialiseModules() {
		for (Map.Entry<String, Module> entry : modules.entrySet()) {
			String moduleName = entry.getKey();
			initialiseModule(moduleName);
		}
	}

	boolean initialiseModule(String moduleName) {
		Module module = modules.get(moduleName);
		if (module.initialise()) {
			return true;
		} else {
			log.error("Failed to initialise module " + moduleName + ".");
			module.defunct = true;
			return false;
		}
	}

	//
	// Module starting
	//
	void startModules() {
		for (Map.Entry<String, Module> entry : modules.entrySet()) {
			String moduleName = entry.getKey();
			startModule(moduleName);
		}
	}

	boolean startModule(String moduleName) {
		Module module = modules.get(moduleName);
		if (module.defunct) {
			log.warning("Module " + moduleName + " is defunct, not starting it.");
			return false;
		} else {
			log.information("Starting module " + moduleName + ".");
			module.start();
			return true;
		}
	}

	//
	// Filter stopping
	//
	void stopFilters() {
		for (Map.Entry<String, Filter> entry : filters.entrySet()) {
			String filterName = entry.getKey();
			stopFilter(filterName);
		}
	}

	boolean stopFilter(String filterName) {
		Filter filter = filters.get(filterName);
		if (filter == null) {
			log.notification("Filter " + filterName + " is not loaded.");
			return false;
		}
		if (filter.stop()) {
			log.information("Filter " + filterName + " stopped.");
			return true;
		} else {
			log.notification("Could not stop filter " + filterName + ".");
			return false;
		}
	}

	//
	// Module stopping
	//
	void stopModules() {
		for (Map.Entry<String, Module> entry : modules.entrySet()) {
			String moduleName = entry.getKey();
			stopModule(moduleName);
		}
	}

	boolean stopModule(String moduleName) {
		Module module = modules.get(moduleName);
		if (module == null) {
			log.notification("Module " + moduleName + " is not loaded.");
			return false;
		}
		if (module.thread.isAlive()) {
			module.stop();
			try {
				module.thread.join(3000);
				if (!module.thread.isAlive()) {
					log.information("Module " + moduleName + " stopped.");
					return true;
				} else {
					log.notification("Could not stop module " + moduleName + ".");
				}
			} catch (InterruptedException e) {
				log.warning("Got interrupted while waiting for module " + moduleName + " to join.");
			}
		} else {
			log.information("Module " + moduleName + " is not alive.");
		}
		return false;
	}

	//
	// Configuration
	//
	void dumpConfiguration() {
		log.verbose("Dumping all read configuration entries.");
		for (Map.Entry<String, String> entry : configuration.getAll().entrySet()) {
			log.verbose(entry.getKey() + " = " + entry.getValue());
		}
	}

	private void reloadConfiguration() {
		// TODO
	}

	//
	// Shutting down
	//
	private void shutdown() {
		stopModules();
		stopFilters();
		stop();
	}

	private void stop() {
		// Shut down logger
		log.information("Stopping Logger...");
		logger.stop();
		log.information("Exiting...");
		System.exit(0);
	}

	//
	// Signal handling
	//
	/**
	 * Register the handled signals.
	 */
	private void registerSignals() {
		try {
			Signal.handle(new Signal("INT"), this);
		} catch (java.lang.IllegalArgumentException e) {
			log.notification("INT Signal not supported on this platform.");
		}
		try {
			Signal.handle(new Signal("HUP"), this);
		} catch (java.lang.IllegalArgumentException e) {
			log.notification("HUP Signal not supported on this platform.");
		}
	}

	/**
	 * Handle a signal.
	 *
	 * @param signal The signal cought.
	 */
	@Override
	public void handle(Signal signal) {
		System.err.println("Cought signal: " + signal.getName());
		switch (signal.getName()) {
			case "INT":
				shutdown();
				break;
			case "HUP":
				// TODO: Reload configuration file
				reloadConfiguration();
				break;
			default:
				System.err.println("No action specified for this signal.");
				break;
		}
	}

}
