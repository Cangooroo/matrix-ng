package matrix.core;

import java.util.LinkedList;

public class CoreModule extends MessageHandlerModule {

	CoreModule(LoggerProxy loggerProxy) {
		super(loggerProxy);
	}

	@Override
	public boolean updateConfiguration(Configuration configuration) {
		return true;
	}

	@Override
	protected boolean initialise() {
		LinkedList<String> filters = new LinkedList<>();
		filters.add("Command");
		updateMessageSubscription(filters);
		return true;
	}

	@Override
	protected boolean handleMessage(Message message) {
		log.verbose("Got message of type " + message.type + "!");
		if (message instanceof CommandMessage)
			System.out.println("Command: " + ((CommandMessage)message).command);
		return true;
	}

	@Override
	protected boolean finalise() {
		return true;
	}

}
