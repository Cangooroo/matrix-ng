package matrix.core;

import org.apache.commons.lang3.exception.ExceptionUtils;

import java.util.Calendar;
import java.util.Date;

/**
 * Event object that can be logged by the {@link Logger}.
 */
public class Event {

	/**
	 * The severity of the {@link Event}.
	 */
	public enum Severity {

		/**
		 * A very serious event, the application will probably die.
		 */
		FATAL(0),

		/**
		 * A serious event, the job will probably fail/abort.
		 */
		ERROR(1),

		/**
		 * An event describing an abnormal behaviour or environment.
		 */
		WARNING(2),

		/**
		 * An event more serious than {@link #INFORMATION} as this message
		 * should be noted.
		 */
		NOTIFICATION(3),

		/**
		 * A normal event, that describes the normal behaviour.
		 */
		INFORMATION(4),

		/**
		 * This severity is used for in-depth understanding of the process, or
		 * debugging.
		 */
		VERBOSE(5),

		/**
		 * These events are only used for debugging, this contain a lot of
		 * probably unneccessary information.
		 */
		DEBUG(6);

		/**
		 * Severity represented with a number.
		 */
		public final int level;

		/**
		 * Create the enum with the numeric level.
		 *
		 * @param level The severity's level with a number.
		 */
		Severity(int level) {
			this.level = level;
		}

		/**
		 * Returns a {@link Severity} for a given level.
		 * @param level The level to search for.
		 * @return The found {@link Severity} or null otherwise.
		 */
		public static Severity getSeverityForLevel(int level) {
			for (Severity severity : Severity.values()) {
				if (severity.level == level) {
					return severity;
				}
			}
			return null;
		}
	}

	/**
	 * Severity of the Event.
	 */
	public final Severity severity;

	/**
	 * The timestamp of the creation.
	 */
	public final Date timestamp;

	/**
	 * The detailed description of the Event.
	 */
	public final String message;

	/**
	 * Constructs the event with the given severity and a message.
	 *
	 * @param severity The severity of the event.
	 * @param message The detailed description of the Event.
	 */
	public Event(Severity severity, String message) {
		this.timestamp = Calendar.getInstance().getTime();
		this.severity = severity;
		this.message = message;
	}

	/**
	 * Constructs the event with the given severity and an exception.
	 *
	 * @param severity The severity of the event.
	 * @param exception The exception associated with the event.
	 */
	public Event(Severity severity, Exception exception) {
		this.timestamp = Calendar.getInstance().getTime();
		this.severity = severity;
		this.message = ExceptionUtils.getStackTrace(exception);
	}

}
