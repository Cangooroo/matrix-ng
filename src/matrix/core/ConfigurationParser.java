package matrix.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * ConfigurationParser parses configuration files and procudes {@link Configuration}.
 */
class ConfigurationParser {

	private static HashSet<String> includedFiles = new HashSet<>();

	// TODO: Logger Support

	static Configuration loadConfiguration(File file) {
		includedFiles = new HashSet<>();
		Configuration configuration = new Configuration();
		// TODO: Parsing time
		try {
			parseConfiguration(configuration, file);
		} catch (FileNotFoundException e) {
			System.err.println("ERROR: File not found: " + e.getMessage());
			return null;
		} catch (IOException e) {
			System.err.println("ERROR: IO Exception while reading file: " + e.getMessage());
			return null;
		}
		return configuration;
	}

	private static void parseConfiguration(Configuration configuration, File file) throws FileNotFoundException, IOException {
		if (includedFiles.contains(file.getAbsolutePath())) {
			System.err.println("WARNING: Already included '" + file.getAbsolutePath() + "', skipping.");
			return;
		}
		BufferedReader reader = new BufferedReader(new FileReader(file));
		includedFiles.add(file.getAbsolutePath());
		for (String originalLine; (originalLine = reader.readLine()) != null;) {
			// Trim the line
			String line = originalLine.trim();
			// Remove comments
			line = line.replaceAll("#.*", "");
			// Drop if empty
			if (line.isEmpty()) {
				continue;
			}
			// The line should be 'key = value' or 'include file' format
			if (line.matches(".*=.*")) {
				Matcher keyValueMatcher = Pattern.compile("\\s*(.*?)\\s*=\\s*(\"(?<I1>.*)\"|'(?<I2>.*)'|(?<I3>.*))").matcher(line);
				if (keyValueMatcher.matches()) {
					String key = keyValueMatcher.group(1);
					String value = null;
					for (String name : Arrays.asList("I1", "I2", "I3")) {
						value = keyValueMatcher.group(name);
						if (value != null) {
							break;
						}
					}
					if (!key.isEmpty() && !value.isEmpty()) {
						configuration.set(key, value);
						continue;
					}
				}
			} else if (line.matches(".*include.*")) {
				Matcher includeFileMatcher = Pattern.compile("\\s*include\\s*(\"(?<I1>.*)\"|'(?<I2>.*)'|(?<I3>.*))").matcher(line);
				if (includeFileMatcher.matches()) {
					String fileName = null;
					for (String name : Arrays.asList("I1", "I2", "I3")) {
						fileName = includeFileMatcher.group(name);
						if (fileName != null) {
							break;
						}
					}
					File filePath = new File(fileName);
					if (!filePath.isAbsolute()) {
						filePath = new File((new File(file.getAbsolutePath())).getParent() + File.separator + fileName);
					}
					parseConfiguration(configuration, filePath);
					continue;
				}
			}
			System.err.println("WARNING: Invalid line '" + originalLine + "' in configuration file '" + file.getPath() + "'");
		}
	}

}
