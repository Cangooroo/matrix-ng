package matrix.core;

import java.io.File;
import java.io.PrintStream;
import java.io.RandomAccessFile;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

class Logger {

	/**
	 * Name of the "module" used in Events.
	 */
	private static final String name = "Core.Logger";

	/**
	 * Configuration for the class.
	 */
	private Configuration configuration = null;

	/**
	 * Severity levels dictionary.
	 */
	private HashMap<String, Integer> severityDictionary = new HashMap<>();

	/**
	 * A {@link PrintStream} for the log file.
	 */
	private PrintStream fileStream = null;

	/**
	 * Date format for the {@link Event}s.
	 */
	private final SimpleDateFormat eventDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	/**
	 * Terminal color escape sequences.
	 */
	private final HashMap<Event.Severity, String> severityColorCodes;

	/**
	 * Constructor.
	 */
	Logger() {
		// Create the severity dictionary
		severityDictionary.put("fatal", 0);
		severityDictionary.put("error", 1);
		severityDictionary.put("warning", 2);
		severityDictionary.put("notification", 3);
		severityDictionary.put("information", 4);
		severityDictionary.put("verbose", 5);
		severityDictionary.put("debug", 6);
		// Terminal color codes for the colorised severity output
		severityColorCodes = new HashMap<>();
		severityColorCodes.put(Event.Severity.FATAL, "\033[31m"); // Red
		severityColorCodes.put(Event.Severity.ERROR, "\033[33m"); // Yellow
		severityColorCodes.put(Event.Severity.WARNING, "\033[m"); // Normal
		severityColorCodes.put(Event.Severity.NOTIFICATION, "\033[35m"); // Purple
		severityColorCodes.put(Event.Severity.INFORMATION, "\033[32m"); // Green
		severityColorCodes.put(Event.Severity.VERBOSE, "\033[36m"); // Cyan
		severityColorCodes.put(Event.Severity.DEBUG, "\033[34m"); // Blue
	}

	final void updateConfiguration(Configuration conf) {
		configuration = conf;
		configuration.setPrefix("Core.Logger.");
		HashMap<String, String> defaults = new HashMap<>();
		defaults.put("stderr.enabled", "true");
		defaults.put("stderr.colors", "true");
		defaults.put("file.enabled", "false");
		defaults.put("file.path", "logs/matrix-%m-%d.log");
		defaults.put("file.mode", "new");
		defaults.put("verbosity", "notification");
		configuration.setDefaults(defaults);
		// Print colors if enabled
		if (configuration.getBoolean("stderr.enabled") && configuration.getBoolean("stderr.colors")) {
			System.err.println("Colors are enabled on standard error output. Color codes are the following: ");
			for (Event.Severity severity : Event.Severity.values()) {
				System.err.print(severityColorCodes.get(severity));
				System.err.print("  " + severity.toString() + " (" + severity.level + ")");
				System.err.println("\033[m");
			}
		}
		if (configuration.getBoolean("file.enabled")) {
			openLogFile();
		}
	}

	void openLogFile() {
		// If the logfile is already open, close first.
		closeLogFile();
		logEvent(new Event(Event.Severity.DEBUG, "Opening log file."));
		File logFilePath = new File(getLogFilePath()).getAbsoluteFile();
		File logFileDirectory = logFilePath.getParentFile();
		if (!logFileDirectory.exists()) {
			logEvent(new Event(Event.Severity.NOTIFICATION, "Path to the logfile (" + logFileDirectory.getAbsolutePath() + ") does not exist. Creating directories."));
			if (!logFileDirectory.mkdirs()) {
				logEvent(new Event(Event.Severity.ERROR, "Failed to create parent directory for the logfile. File logging is disabled."));
				configuration.set("file.enabled", "false");
				return;
			}
		}
		try {
			if (!logFilePath.createNewFile()) {
				switch (configuration.get("file.mode").toLowerCase()) {
					case "append":
						logEvent(new Event(Event.Severity.INFORMATION, "The log file already exists. I will append to it."));
						fileStream = new PrintStream(logFilePath);
						break;
					case "overwrite":
						logEvent(new Event(Event.Severity.INFORMATION, "The log file already exists. It will be overwritten."));
						new RandomAccessFile(logFilePath, "rw").setLength(0);
						fileStream = new PrintStream(logFilePath);
						break;
					case "new":
						// TODO: create a new file
						break;
					default:
						logEvent(new Event(Event.Severity.INFORMATION, "The log file already exists, and the configuration says '" + configuration.get("file.mode") + "' that I don't understand. File logging is disabled."));
						configuration.set("file.enabled", "false");
						return;
				}
			} else {
				fileStream = new PrintStream(logFilePath);
			}
		} catch (Exception e) {
			logEvent(new Event(Event.Severity.ERROR, "Failed to open/create log file. File logging is disabled."));
			logEvent(new Event(Event.Severity.ERROR, e));
			configuration.set("file.enabled", "false");
			return;
		}
		// Test writing a line
		fileStream.println("Logger session opened at " + eventDateFormat.format(Calendar.getInstance().getTime()));
		if (fileStream.checkError()) {
			logEvent(new Event(Event.Severity.ERROR, "Probably failed to write the first line into the log file. File logging is disabled."));
			configuration.set("file.enabled", "false");
		}
	}

	void closeLogFile() {
		if (fileStream != null) {
			logEvent(new Event(Event.Severity.DEBUG, "Closing log file."));
			fileStream.close();
			fileStream = null;
		}
	}

	void stop() {
		configuration.set("file.enabled", "false");
		closeLogFile();
	}

	private void logEvent(Event event) {
		logEvent(Logger.name, event);
	}

	synchronized void logEvent(String sourceName, Event event) {
		String line = eventDateFormat.format(event.timestamp)
					+ " [" + event.severity.name() + "] (" + sourceName + ") "
					+ event.message;
		if (configuration.getBoolean("file.enabled") && event.severity.level <= getFileVerbosity()) {
			if (fileStream != null) {
				fileStream.println(line);
			} else {
				if (!sourceName.equals(Logger.name)) {
					System.err.println("ERROR: File logging is enabled, but no file is open. WTF? File logging is disabled.");
					configuration.set("file.enabled", "false");
					logEvent(new Event(Event.Severity.ERROR, "File logging is enabled, but no file is open. WTF? File logging is disabled."));
				}
			}
		}
		if (configuration.getBoolean("stderr.enabled") && event.severity.level <= getStderrVerbosity()) {
			if (configuration.getBoolean("stderr.colors")) {
				String coloredSeverity = "[" + severityColorCodes.get(event.severity) + event.severity.name() + "\033[m" + "]";
				line = line.replaceAll("\\[" + event.severity.name() + "\\]", coloredSeverity);
			}
			System.err.println(line);
		}
	}

	int getStderrVerbosity() {
		if (configuration.get("stderr.verbosity") != null) {
			return configuration.getCustomInteger("stderr.verbosity", severityDictionary, false);
		} else {
			return configuration.getCustomInteger("verbosity", severityDictionary, false);
		}
	}

	int getFileVerbosity() {
		if (configuration.get("file.verbosity") != null) {
			return configuration.getCustomInteger("file.verbosity", severityDictionary, false);
		} else {
			return configuration.getCustomInteger("verbosity", severityDictionary, false);
		}
	}

	String getLogFilePath() {
		// TODO: Date escapes
		return configuration.get("file.path");
	}

}
