package matrix.core;

public class CommandMessage extends Message {

	public final String command;

	public CommandMessage(String command) {
		super("Command");
		this.command = command;
	}

}
