package matrix.core;

import java.util.concurrent.LinkedBlockingQueue;

/**
 * A message queue class that is basically a concurrently accessible linked
 * list of {@link Message}s. The messages are inserted and removed in FIFO order.
 */
public class MessageQueue {

	/**
	 * A thread-safe linked queue.
	 */
	private LinkedBlockingQueue<Message> messages = new LinkedBlockingQueue<>();

	/**
	 * Add a {@link Message} to the queue.
	 * @param message The {@link Message} to be added.
	 * @return true {@see Collection#add(java.lang.Object)}
	 */
	boolean addMessage(Message message) {
		return messages.offer(message);
	}

	/**
	 * Removes a {@link Message} from the queue or return null if there is none.
	 * @return The removed {@link Message} or null if the queue is empty.
	 */
	public Message removeMessage() {
		return messages.poll();
	}

	/**
	 * Remove a {@link Message} from the queue or wait until one is available.
	 * @return The removed {@link Message}.
	 * @throws InterruptedException
	 */
	public Message waitAndRemoveMessage() throws InterruptedException {
		return messages.take();
	}

}
