package matrix.modules;

import matrix.core.Configuration;
import matrix.core.LoggerProxy;
import matrix.core.Module;

import java.util.HashMap;

/**
 *
 * @author Administrator
 */
public class Statistic extends Module{
	private class ModuleStatistics {
		public int messageQueueMaximumDelay = 0;
		public int messageQueueMaximumLenght = 0;
		public double messageQueueAvarageDelay = 0;
		public double messageQueueAvarageLenght = 0;
		public double load = 0;
	}
	private class MessageStatistics {
		public int sentNumber;
		public int messageFiltered;
		public int recievedNumber;
	}
	private class FilterStatistics {
		//Light
		public int messageFiltered;
		public double minDelay;
		public double avgDelay;
		public double maxDelay;
		//Heavy
		public double load;
		public int minQue;
		public double avgQue;
		public int maxQue;
	}
	private static HashMap<Module, ModuleStatistics> modules = new HashMap<>();

	public Statistic(LoggerProxy loggerProxy) {
		super(loggerProxy);
	}
	@Override
	protected void mainLoop() {
		log.information("Hello from mainLoop()");
		while (true) {
//			try {
//				// Do nothing
//				this.wait(25);
//			} catch (InterruptedException ex) {
//				Logger.getLogger(DummyModule.class.getName()).log(Level.WARNING, null, ex);
//			}
		}
	}

	@Override
	protected boolean initialise() {
		log.information("Hello from initialise()");
		return true;
	}

	@Override
	public boolean updateConfiguration(Configuration configuration) {
		log.information("Hello from updateConfiguration()");
		return true;
	}

	@Override
	protected boolean finalise() {
		log.information("Hello from finalise()");
		return true;
	}


}
