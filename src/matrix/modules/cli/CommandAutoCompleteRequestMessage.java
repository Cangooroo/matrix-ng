package matrix.modules.cli;

import matrix.core.Message;

class CommandAutoCompleteRequestMessage extends Message {

	private final String line;
	private final int cursor;

	public CommandAutoCompleteRequestMessage(String line, int cursor) {
		super("cli.autocomplete.request");
		this.line = line;
		this.cursor = cursor;
	}
}
