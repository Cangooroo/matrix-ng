package matrix.modules.cli;

import jline.Completor;
import jline.ConsoleReader;
import matrix.core.Configuration;
import matrix.core.LoggerProxy;
import matrix.core.Message;
import matrix.core.Module;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class CLIModule extends Module {

	private ConsoleReader consoleReader;

	public CLIModule(LoggerProxy loggerProxy) {
		super(loggerProxy);
	}

	@Override
	public boolean updateConfiguration(Configuration configuration) {
		return true;
	}

	@Override
	protected boolean initialise() {
		LinkedList filters = new LinkedList<String>();
		filters.add("cli.autocomplete.reply");
		updateMessageSubscription(filters);
		try {
			consoleReader = new ConsoleReader();
			consoleReader.addCompletor(new Completor() {
				@Override
				public int complete(String line, int cursor, List list) {
					return completeCommand(line, cursor, list);
				}
			});
		} catch (IOException e) {
			log.error("Failed to create ConsoleReader.");
			log.error(e);
			return false;
		}
		return true;
	}

	int completeCommand(String line, int cursor, List list) {
		// Send request to Modules
		Message request = new CommandAutoCompleteRequestMessage(line, cursor);
		sendMessage(request);
		// Wait for replies to come
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			// We don't care if we are interrupted.
		}
		// Handle all replies
		Message reply;
		while ((reply = messageQueue.removeMessage()) != null) {
			if (reply.type == "cli.autocomplete.reply") {
				if (reply.getRID() == request.ID) {
					CommandAutoCompleteReplyMessage ccrm = (CommandAutoCompleteReplyMessage) reply;
					list.add(ccrm.completion);
				} else {
					log.information("Dropped late autocomplete reply message with ID = " + reply.ID);
				}
			} else {
				log.notification("Ignored message with ID = " + reply.ID + ", type = " + reply.type);
			}
		}
		// Search the last word's beginning
		if (cursor >= line.length()) {
			cursor--;
		}
		while (cursor > 0 && line.charAt(cursor - 1) != ' ') {
			cursor--;
		}
		return cursor;
	}

	@Override
	protected void mainLoop() {
		String line;
		try {
			while ((line = consoleReader.readLine("> ")) != null) {
				System.out.println(line);
			}
		} catch (IOException e) {
			log.error("Error while reading line from console.");
			log.error(e);
		}
	}

	@Override
	protected boolean finalise() {
		return true;
	}
}
