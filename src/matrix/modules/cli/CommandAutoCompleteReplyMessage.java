package matrix.modules.cli;

import matrix.core.Message;

public class CommandAutoCompleteReplyMessage extends Message {

	public final String completion;

	public CommandAutoCompleteReplyMessage(String completion) {
		super("cli.autocomplete.reply");
		this.completion = completion;
	}
}
