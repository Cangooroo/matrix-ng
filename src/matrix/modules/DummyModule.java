package matrix.modules;

import matrix.core.Configuration;
import matrix.core.LoggerProxy;
import matrix.core.Message;
import matrix.core.Module;
import matrix.modules.cli.CommandAutoCompleteReplyMessage;

import java.util.LinkedList;

public class DummyModule extends Module {

	public DummyModule(LoggerProxy loggerProxy) {
		super(loggerProxy);
	}

	@Override
	protected void mainLoop() {
		log.information("Hello from mainLoop()");
		while (true) {
			try {
				Message m = messageQueue.waitAndRemoveMessage();
				switch (m.type) {
					case "cli.autocomplete.request":
						sendMessage(new CommandAutoCompleteReplyMessage("foo").replyTo(m));
						sendMessage(new CommandAutoCompleteReplyMessage("fuck").replyTo(m));
						break;
				}
			} catch (InterruptedException e) {
				return;
			}
		}
	}

	@Override
	protected boolean initialise() {
		log.information("Hello from initialise()");
		LinkedList<String> list = new LinkedList<>();
		list.add("cli.autocomplete.request");
		updateMessageSubscription(list);
		return true;
	}

	@Override
	public boolean updateConfiguration(Configuration configuration) {
		log.information("Hello from updateConfiguration()");
		return true;
	}

	@Override
	protected boolean finalise() {
		log.information("Hello from finalise()");
		return true;
	}


}
