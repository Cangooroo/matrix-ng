package matrix.filters;

import matrix.core.CommandMessage;
import matrix.core.Configuration;
import matrix.core.Filter;
import matrix.core.LoggerProxy;
import matrix.core.Message;

public class TestFilter extends Filter {

	private String fillText1 = "[";
	private String fillText2 = "]";
	
	public TestFilter(LoggerProxy loggerProxy) {
		super(loggerProxy);
		setMessageType("Command");
	}
	
	protected Message filterMessage(final Message message) {
		if (!(message instanceof CommandMessage)) return message;
		return new CommandMessage(fillText1+((CommandMessage)message).command+fillText2);
	}

	public boolean updateConfiguration(Configuration configuration) {
		String prop = configuration.get("TestFilter.fillText1");
		System.out.println(prop);
		if (prop != null) fillText1 = prop;
		prop = configuration.get("TestFilter.fillText2");
		if (prop != null) fillText2 = prop;
		return true;
	}
}
